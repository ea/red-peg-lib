/* Red-Peg slave library

  allows easier (smaller overhead) reading of red-peg
  sensors by the attached microcontroller board, using
  the onboard ATmega328 on the red-peg shield
*/
#ifndef red_peg_slave_h
#define red_peg_slave_h

#include "red_peg.h"
#include "RTClib.h"
#include "MCP342x.h"

// pin number connections for the slave micro
#define SS_SLAVE_PIN 10
#define SLAVE_SENSOR_ACTIVE_PIN 2

class red_peg_slave
{
private:
  volatile uint8_t _command_head_index = 0; // up to BUFFER_LEN
  volatile uint8_t _command_tail_index = 0; // up to BUFFER_LEN
  volatile uint8_t _command_record_index = 0; // up to size_t_sensorData
  volatile uint8_t _data_head_index = 0; // up to BUFFER_LEN
  volatile uint8_t _data_tail_index = 0; // up to BUFFER_LEN
  volatile uint8_t _data_record_index = 0; // up to size_t_sensorData
  //volatile uint32_t _last_received_data = 0; // to know about new transmissions
  volatile uint8_t _ss_pin_state = LOW;

  uint8_t _ss_pin;
  const size_t size_t_SensorData = sizeof(t_SensorData);

  // need some buffers to hold the incoming/outgoing data
  t_SensorData _command_buffer[BUFFER_LEN];
  t_SensorData _data_buffer[BUFFER_LEN];
  // and the uint8_t* equivalents for byte-by-byte shifting
  uint8_t* _command_buff = (uint8_t*)(&_command_buffer);
  uint8_t* _data_buff = (uint8_t*)(&_data_buffer);


  void a_peek(t_SensorData *sensor_data);
  void new_command_line();

public:

  void handle_interrupt(); // reset the buffer positions on SPI SS pin activation
  void begin(uint8_t ss_pin = SS_SLAVE_PIN);
  void spi_stc_isr(); // for: ISR (SPI_STC_vect)
  bool spiComplete(); // check SPI is inactive
  int8_t available(); // return the number of available records
  t_SensorData read(); // get the next item from the incomming buffer
  t_SensorData peek(); // look at the next item in the incoming buffer
  int8_t write(t_SensorData return_data); // put a dataset on the send buffer
  void print_data(t_SensorData data_record); // serial.print the data record
  char* sensor_message(sensor_e request); // return a human readable version of the enum
};

#endif // red_peg_slave_h
