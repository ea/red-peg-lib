#include <inttypes.h>
#include "Arduino.h"

#include "red_peg_slave.h"

// slave only functions

void red_peg_slave::begin(uint8_t ss_pin)
{
  // set the slave select pin
  _ss_pin = ss_pin;
  pinMode(_ss_pin, INPUT);
  // turn on SPI in slave mode
  SPCR |= bit (SPE);
  // prep the first byte in the SPI data record buffer
  SPDR = (uint8_t)_data_buff[(_data_tail_index*size_t_SensorData)+_data_record_index];
  // find out the current SS state
  pinMode(SS_SLAVE_PIN, INPUT);
  handle_interrupt();
  // now turn on interrupts
  SPI.attachInterrupt();
  // make sure the buffers start empty
  for (uint8_t i=0; i<sizeof(_command_buff); i++) {
    _command_buff[i] = END_BYTE;
    _data_buff[i] = END_BYTE;
  }
}

void red_peg_slave::handle_interrupt()
{
  // check which way the MISO pin should be set
  uint8_t current_ss_state = digitalRead(SS_SLAVE_PIN);
  if (current_ss_state != _ss_pin_state) {
    if (current_ss_state == LOW) {
      // activate MISO as output, only if our SS pin is low (active)
      pinMode(MISO, OUTPUT);
      _ss_pin_state = LOW;
      new_command_line();
      //Serial.println(F("SS LOW"));
    } else {
      pinMode(MISO, INPUT);
      _ss_pin_state = HIGH;
      //Serial.println(F("SS HIGH"));
    }
  }
}

void red_peg_slave::new_command_line()
{
  // we've started a new line,
  // so…
  // set to the start of the next empty line in the command buffer
  if ((_command_head_index+1)%BUFFER_LEN != _command_tail_index) {
    _command_head_index = (_command_head_index+1) % BUFFER_LEN;
    // or repeat the same line if there's no space in the buffer
  }
  // and set the record index to the beginning
  _command_record_index = 0;
  // if we're at the data buffer head
  if (_data_tail_index != _data_head_index) {
    // increment to the next line
    _data_tail_index = (_data_tail_index+1) % BUFFER_LEN;
    // we'll just repeat the head if there's no more data
  }
  // set the _data_record_index to zero
  _data_record_index = 0;
  // put the byte into the SPI buffer ready for transmission
  //SPDR = byte(_data_buff[(_data_tail_index*size_t_SensorData)+_data_record_index]);
  SPDR = _data_buff[(_data_tail_index*size_t_SensorData)+_data_record_index];
}

bool red_peg_slave::spiComplete()
{
  uint8_t current_state = digitalRead(_ss_pin);
  if(current_state == HIGH) {
    return true;
  } else {
    return false;
  }
}

int8_t red_peg_slave::available()
{
  int8_t num = 0;
  // take the item from the incoming buffer and parse for the action
  uint8_t tail_position = _command_tail_index;
  if (spiComplete()) {
    while (tail_position != _command_head_index) {
      tail_position = (tail_position + 1) % BUFFER_LEN;
      num++;
    }
  }
  return num;
}

t_SensorData red_peg_slave::read()
{
  t_SensorData buffer;
  if (_command_tail_index != _command_head_index) {
    // make a peek
    a_peek(&buffer);
    // and then increment the buffer
    _command_tail_index = (_command_tail_index + 1) % BUFFER_LEN;
    // and return the result
  }
  return buffer;
}

t_SensorData red_peg_slave::peek()
{
  t_SensorData buffer;
  if (_command_tail_index != _command_head_index) {
    // make a peek
    a_peek(&buffer);
  }
  return buffer;
}

void red_peg_slave::a_peek(t_SensorData *sensor_data)
{
  // take the next reading
  uint8_t next_record = (_command_tail_index + 1) % BUFFER_LEN;
  *sensor_data = _command_buffer[next_record];
}

int8_t red_peg_slave::write(t_SensorData return_data)
{
  // increment the _data_buffer
  uint8_t next_record = (_data_head_index + 1) % BUFFER_LEN;
  // check there's space on the buffer to add that data
  //TODO: this should probably be replaced by a memmove()
  if (next_record != _data_tail_index) {
    _data_buffer[next_record].the_sensor = return_data.the_sensor;
    _data_buffer[next_record].y= return_data.y;
    _data_buffer[next_record].m = return_data.m;
    _data_buffer[next_record].d = return_data.d;
    _data_buffer[next_record].hh = return_data.hh;
    _data_buffer[next_record].mm = return_data.mm;
    _data_buffer[next_record].ss = return_data.ss;
    _data_buffer[next_record].the_reading = return_data.the_reading;
    //_data_buffer[next_record].the_data = return_data.the_data;
    _data_head_index = next_record;
    return true; // ok
  } else {
    return false; // buffer is already full
  }
}

void red_peg_slave::spi_stc_isr()
{
  // we just received a byte
  _command_buff[(_command_head_index*size_t_SensorData)+_command_record_index] = SPDR;
  // increment the _command_record_index
  _command_record_index++;
  if (_command_record_index >= size_t_SensorData) {
    // reset to beginning if there's too much data sent
    _command_record_index = 0;
  }
  // increment the _data_record_index
  _data_record_index = (_data_record_index+1) % size_t_SensorData;
  if (_data_record_index >= size_t_SensorData) {
    // we've rolled over to the next one, ignore the rest,
    // the sender needs to toggle the SS for the next data line
    SPDR = END_BYTE;
  } else {
    //SPDR = byte(_data_buff[(_data_tail_index*size_t_SensorData)+_data_record_index]);
    SPDR = (uint8_t)_data_buff[(_data_tail_index*size_t_SensorData)+_data_record_index];
  }
}

void red_peg_slave::print_data(t_SensorData data_record)
{
  // helper function to write out the data to serial
  Serial.print("0x");
  Serial.print(data_record.start_byte, HEX);
  Serial.write(',');
  Serial.print(sensor_message(data_record.the_sensor));
  Serial.write(',');
  Serial.print(data_record.y);
  Serial.write('-');
  Serial.print(data_record.m);
  Serial.write('-');
  Serial.print(data_record.d);
  Serial.write('T');
  Serial.print(data_record.hh);
  Serial.write(':');
  Serial.print(data_record.mm);
  Serial.write(':');
  Serial.print(data_record.ss);
  Serial.write('Z');
  Serial.write(',');
  Serial.print(data_record.the_reading);
  //Serial.write(',');
  //Serial.print(data_record.the_data);
  Serial.print(",0x");
  Serial.print(data_record.end_byte, HEX);
  Serial.println();
}

char* red_peg_slave::sensor_message(sensor_e request)
{
  if (request == OK) {
    return "OK";
  } else if (request == AVAILABLE) {
    return "AVAILABLE";
  } else if (request == EMPTY) {
    return "EMPTY";
  } else if (request == RTC) {
    return "RTC";
  } else if (request == ADC1) {
    return "ADC1";
  } else if (request == ADC2) {
    return "ADC2";
  } else if (request == ADC3) {
    return "ADC3";
  } else if (request == SDI12) {
    return "SDI12";
  } else {
    return "UNKNOWN";
  }
}
